package com.zncd.recruitsystem.entity;

import java.io.Serializable;

public class UserEntity implements Serializable{
    private static final long serialVersionUID = 1L;
    private Long id;
    private String studentID;
    private String password;
    private String tel1;
    private String email;
    private String usersSex;
//    private Userinfo userinfo;
//    public Userinfo getUserinfo() {
//        return userinfo;
//    }
//    public void setUserinfo(Userinfo userinfo) {
//        this.userinfo = userinfo;
//    }
    public String getUsersSex() {
        return usersSex;
    }
    public void setUsersSex(String userSex) {
        this.usersSex = userSex;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getStudentId() {
        return studentID;
    }
    public void setStudentId(String studentId) {
        this.studentID = studentId;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getTel1() {
        return tel1;
    }
    public void setTel1(String tel) {
        this.tel1 = tel;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
}
