package com.zncd.recruitsystem.entity;

import java.io.Serializable;

public class TestEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long testid;
    private String testname;
    public Long getTestid() {
        return testid;
    }

    public void setTestid(Long testid) {
        this.testid = testid;
    }

    public String getTestname() {
        return testname;
    }

    public void setTestname(String testname) {
        this.testname = testname;
    }


}
