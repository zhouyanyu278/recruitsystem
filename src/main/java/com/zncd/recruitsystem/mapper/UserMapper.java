package com.zncd.recruitsystem.mapper;
import java.util.List;

import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.zncd.recruitsystem.entity.UserEntity;;
public interface UserMapper {
    @Select("select * from users")
    @Results({

            @Result(property="tel1",column="tel")
    })
    List<UserEntity> getAll();
    @Select("select * from users where users_sex=#{sex}")
    @Results({

            @Result(property="tel1",column="tel")
    })
    List<UserEntity> getsex(String sex);

    @Select("select * from users where id=#{id}")
    @Results({
            @Result(property="tel1",column="tel"),
            @Result(property="userinfo",column="id",one=@One(select="com.example.demo.mapper.UserinfoMapper.getcontent"))
    })
    UserEntity getUserById(Long id);
}
