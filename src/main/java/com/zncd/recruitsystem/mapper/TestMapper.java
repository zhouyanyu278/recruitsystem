package com.zncd.recruitsystem.mapper;

import com.zncd.recruitsystem.entity.TestEntity;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface TestMapper {
    @Select("select * from test")
    public List<TestEntity> getall();
}
