package com.zncd.recruitsystem.controller;

import com.zncd.recruitsystem.entity.TestEntity;
import com.zncd.recruitsystem.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/test")
public class TestController {
    @Autowired
    private TestService testService;
    //
    @RequestMapping("/test1")
    public List<TestEntity> testaction(@RequestParam(value = "id",required = false) Integer id){
        return testService.getall();
    }

}
