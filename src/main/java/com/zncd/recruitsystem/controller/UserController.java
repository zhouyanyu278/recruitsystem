package com.zncd.recruitsystem.controller;

import com.zncd.recruitsystem.entity.UserEntity;
import com.zncd.recruitsystem.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userservice;
    @RequestMapping("/users")
    public List<UserEntity> getuers(){
        return userservice.getAllUser();
    }
    @RequestMapping("/sex")
    public List<UserEntity> getsex(@RequestParam(value="sex",defaultValue="boy") String sex){
        return userservice.getsex(sex);

    }
    @RequestMapping("/userinfo")
    public UserEntity getUserinfoByid(@RequestParam(value="userid",defaultValue="9") Long id) {
        return userservice.getUserByid(id);
    }
}
