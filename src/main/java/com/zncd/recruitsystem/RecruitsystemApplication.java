package com.zncd.recruitsystem;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@MapperScan("com.zncd.recruitsystem.mapper")
@SpringBootApplication
public class RecruitsystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(RecruitsystemApplication.class, args);
    }

}
