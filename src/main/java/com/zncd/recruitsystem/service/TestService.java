package com.zncd.recruitsystem.service;

import com.zncd.recruitsystem.entity.TestEntity;
import com.zncd.recruitsystem.mapper.TestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestService {
    @Autowired
    private TestMapper testMapper;
    public List<TestEntity> getall(){
        return testMapper.getall();
    }
}
