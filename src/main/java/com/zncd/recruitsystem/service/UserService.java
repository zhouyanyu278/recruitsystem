package com.zncd.recruitsystem.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zncd.recruitsystem.entity.UserEntity;
import com.zncd.recruitsystem.mapper.UserMapper;
@Service
public class UserService {
    @Autowired
    private UserMapper usermapper;
    public List<UserEntity> getAllUser(){

        return usermapper.getAll();
    }
    public List<UserEntity> getsex(String sex){
        return usermapper.getsex(sex);
    }
    public UserEntity getUserByid(Long id){
        return usermapper.getUserById(id);
    }
}